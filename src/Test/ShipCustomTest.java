package Test;

import Component.Elements.Coordinate.Coordinate;
import Component.Elements.Piece.ShipCustom;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShipCustomTest {

    @Test
    public void Contruct11lenghtCustomShip() {

        ShipCustom customShip = new ShipCustom(11);
        assertEquals(customShip.getLenght(),10);
    }
    @Test
    public void Contruct10lenghtCustomShip() {

        ShipCustom customShip = new ShipCustom(10);
        assertEquals(customShip.getLenght(),10);
    }
    @Test
    public void Contruct0lenghtCustomShip() {

        ShipCustom customShip = new ShipCustom(0);
        assertEquals(customShip.getLenght(),2);
    }
    @Test
    public void Contruct1lenghtCustomShip() {

        ShipCustom customShip = new ShipCustom(1);
        assertEquals(customShip.getLenght(),2);
    }
    @Test
    public void ContructNetativelenghtCustomShip() {

        ShipCustom customShip = new ShipCustom(-2);
        assertEquals(customShip.getLenght(),2);
    }
}
