package Test;

import Component.Elements.Coordinate.Coordinate;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class CoordinateTest {


    @Test
    public void testgetHorizontalCoordRight() {

        Coordinate coord = new Coordinate((char) 66,1);
        Coordinate coorXRight = new Coordinate((char) 67 ,1);

        assertEquals(coord.getHorizontalCoord(coord,"right"),coorXRight);
    }
    @Test
    public void testgetHorizontalCoordLeft() {

        Coordinate coord = new Coordinate((char) 66,1);
        Coordinate coorXLeft = new Coordinate((char) 65,1);

        assertEquals(coord.getHorizontalCoord(coord,"left"),coorXLeft);
    }
    @Test
    public void testgetVerticalCoordTop() {

        Coordinate coord = new Coordinate((char) 66,1);
        Coordinate coordYTop = new Coordinate((char) 66 ,2);

        assertEquals(coord.getVerticalCoord(coord,"top"),coordYTop);
    }
    @Test
    public void testgetVerticalCoordDown() {

        Coordinate coord = new Coordinate((char) 66,2);
        Coordinate coordYTop = new Coordinate((char) 66 ,1);

        assertEquals(coord.getVerticalCoord(coord,"down"),coordYTop);
    }

    @Test
    public void ConstructorXtooLow() {

        Coordinate coord = new Coordinate((char) 64,1);
        assertEquals(coord,new Coordinate((char) 65,1));
    }

    @Test
    public void ConstructorYNetative() {

        Coordinate coord = new Coordinate((char) 65,-5);
        assertEquals(coord,new Coordinate((char) 65,1));
    }

    @Test
    public void ConstructorXTooLowAndYNetative() {

        Coordinate coord = new Coordinate((char) 64,-5);
        assertEquals(coord,new Coordinate((char) 65,1));
    }

    @Test
    public void ConstructorNativeCharX() {

        Coordinate coord = new Coordinate('A',1);
        assertEquals(coord,new Coordinate((char) 65,1));
    }

}
