package Test;

import Component.Elements.Options.Option;
import Component.Elements.Piece.ShipCustom;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OptionTest {

    @Test
    public void SetCheckBoardSizeTooLow() {

        Option option = new Option();
        option.setCheckBoardSize(0);
        assertEquals(10,option.getCheckBoardSize());
    }
    @Test
    public void SetCheckBoardSizeTooHigh() {

        Option option = new Option();
        option.setCheckBoardSize(15);
        assertEquals(14,option.getCheckBoardSize());
    }
    @Test
    public void SetCheckBoardSizeNegative() {

        Option option = new Option();
        option.setCheckBoardSize(-5);
        assertEquals(10,option.getCheckBoardSize());
    }

}
