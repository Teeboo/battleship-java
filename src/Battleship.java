import Component.Elements.Deck.ShipDeck;
import Component.Elements.Options.Option;
import Component.Elements.Options.OptionGPanel;
import Component.Listener.*;
import Component.Panel.EndPanel;
import Component.Panel.Gameplay;
import Component.Panel.Menu;
import Component.Panel.PiecePlacement;
import Component.Sound.Sfx;
import Component.Sound.Sound;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.awt.*;

public class Battleship extends JFrame implements MenuListener, OptionListener, PiecePlacementListener, GameplayListener, EndingPanelListener {

    private JPanel contentPanel = (JPanel) getContentPane();
    private Menu menu = new Menu();
    private Option option = new Option();
    private OptionGPanel optionPanel = new OptionGPanel(option);
    private PiecePlacement piecePlacementMenuPlayer1 ;
    private Gameplay gameplayPanel;
    private EndPanel endingPanel;
    private Sound son = new Sound();
    private Sfx sfx = new Sfx();

    public static void main(String[] args) {
        Battleship battleship = new Battleship();
    }

    public Battleship()  {

        setBounds(100, 100, 1500, 1000);
        setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        contentPanel.setLayout(new BorderLayout());

        menu.addMenuListener(this);
        optionPanel.addOptionListener(this);
        launchMenu();

    }

    public void launchMenu(){
        contentPanel.add(menu);

        try {
            son.playMusic("src/Component/Sound/Music.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void PlayButtonPressed() {
        System.out.println("Piece placement menu");
        contentPanel.remove(menu);
        this.piecePlacementMenuPlayer1 = new PiecePlacement(option);
        contentPanel.add(piecePlacementMenuPlayer1);
        piecePlacementMenuPlayer1.addPiecePlacementListener(this);
        this.revalidate();
        this.repaint();

    }
    @Override
    public void OptionButtonPressed() {
        System.out.println("Option menu");
        try {
            sfx.playSfx("src/Component/Sound/SfxStock/NavalFix.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        contentPanel.remove(menu);
        contentPanel.add(optionPanel);
        optionPanel.repaintOptionPanel();
        System.out.println(option.getShipCustomListNumber());
        this.revalidate();
        this.repaint();
    }

    @Override
    public void QuitButtonPressed() {
        this.dispose();
    }

    @Override
    public void validateButtonPressed() {
        System.out.println("Modifications registered");
        contentPanel.remove(optionPanel);
        contentPanel.add(menu);
        this.revalidate();
        this.repaint();
    }

    @Override
    public void ReturnButtonPressedFromPiecePlacement() {
        System.out.println("Back to Main menu");
        option.clearOption();
        contentPanel.remove(piecePlacementMenuPlayer1);
        contentPanel.add(menu);
        this.revalidate();
        this.repaint();
    }

    @Override
    public void ValidateButtonPressedFromPiecePlacement() {

        ShipDeck iaDeck = new ShipDeck();
        gameplayPanel = new Gameplay(option,piecePlacementMenuPlayer1.getPlayerShipDeck());
        System.out.println("Gameplay Start");

        son.stopMusic();
        try {
            son.playMusic("src/Component/Sound/CombatMusic.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        contentPanel.remove(piecePlacementMenuPlayer1);
        contentPanel.add(gameplayPanel);
        this.revalidate();
        this.repaint();
        gameplayPanel.addPiecePlacementListener(this);
    }

    @Override
    public void ReturnButtonPressedFromGameplay() {
        System.out.println("Back to Main menu");
        son.stopMusic();
        option.clearOption();
        contentPanel.remove(gameplayPanel);
        contentPanel.add(menu);
        try {
            son.playMusic("src/Component/Sound/Music.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        this.revalidate();
        this.repaint();
    }

    @Override
    public void GameFinished(String winnerName) {
        System.out.println("GAME FINISH BY "+winnerName);
        son.stopMusic();
        try {
            sfx.playSfx("src/Component/Sound/SfxStock/Peace.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        //endingPanel = new EndPanel(winnerName);
        //contentPanel.remove(gameplayPanel);
        //contentPanel.add(endingPanel);
        //endingPanel.addEndingPanelListener(this);
        //this.revalidate();
        //this.repaint();
    }

    @Override
    public void ReturnPressedFromEndingPanel() {
        contentPanel.remove(endingPanel);
        option.clearOption();
        contentPanel.add(menu);
        try {
            son.playMusic("src/Component/Sound/Music.wav");
        } catch (LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        this.revalidate();
        this.repaint();
    }
}
