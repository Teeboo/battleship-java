package Component.IA;

import Component.Elements.Checkerboard;
import Component.Elements.Deck.ShipDeck;
import Component.Elements.Options.Option;
import Component.Elements.Piece.Piece;
import Component.Elements.Square.SquareGRectangle;
import java.awt.*;
import java.util.LinkedList;
import java.util.Random;

public class Ia {

    private Checkerboard playerCheckerboard;
    private Option option;
    private ShipDeck playerDeck;

    // CONSTRUCTOR

    public Ia(Checkerboard playerCheckerboard, Option option, ShipDeck playerDeck) {
        this.playerCheckerboard = playerCheckerboard;
        this.option = option;
        this.playerDeck = playerDeck;
    }

    // FUNCTION

    public void PlayTurn(){
        if(option.getIaDifficulty().equals("Normal")){

            LinkedList squareList = playerCheckerboard.getSquareList();
            LinkedList<SquareGRectangle> upatdedSquareList = new LinkedList<SquareGRectangle>();
            Random randomInt = new Random();

            for(int i = 0 ; i < squareList.size(); i++){

                SquareGRectangle  square = (SquareGRectangle) squareList.get(i);
                if(square.isUsable()){
                    upatdedSquareList.add(square);
                }
            }

            SquareGRectangle Choicedsquare = upatdedSquareList.get(randomInt.nextInt(upatdedSquareList.size()));

            if(Choicedsquare.isTaken()){

                Choicedsquare.disableButton();
                if (option.getCheckBoardSize() >= 11) {
                    Choicedsquare.applyShipFireImage("Lower");

                }else {
                    Choicedsquare.applyShipFireImage("Normal");
                }

                Piece touchedPiece = playerDeck.getPieceFromCoord(Choicedsquare.getCoordinate());
                touchedPiece.setDestroyed(true);
            }
            else{
                if (option.getCheckBoardSize() >= 11) {
                    Choicedsquare.applyMissImage("Lower");

                }else {
                    Choicedsquare.applyMissImage("Normal");
                }
                Choicedsquare.disableButton();
            }
        }
    }
}
