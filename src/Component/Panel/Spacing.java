package Component.Panel;

import javax.swing.*;
import java.awt.*;

public class Spacing extends JPanel {

    private int width ;
    private int length;
    private Color color;

    public Spacing(int width, int length) {
        this.width = width;
        this.length = length;
        initComponents();
    }

    private void initComponents(){

        this.setLayout(new FlowLayout());
        this.setBackground(Color.DARK_GRAY);
        this.setPreferredSize(new Dimension(width, length));

    }

}


