package Component.Panel;

import Component.Elements.Button.LongButton;
import Component.Elements.Checkerboard;
import Component.Elements.Coordinate.Coordinate;
import Component.Elements.Deck.ShipDeck;
import Component.Elements.Options.Option;
import Component.Elements.Piece.Piece;

import Component.Elements.Piece.ShipCustom;
import Component.Elements.Square.Form;
import Component.Elements.Square.SquareGRectangle;
import Component.Listener.PiecePlacementListener;
import Component.Sound.Sfx;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class PiecePlacement extends JPanel implements ActionListener, MouseListener{

    private Set<PiecePlacementListener> piecePlacementListener = new HashSet<>();
    private final Spacing spacingPanelRight = new Spacing(400,400);
    private final Spacing spacingPanelLeft = new Spacing(400,400);
    private final Spacing spacingPanelNorth = new Spacing(0,40);
    private final JPanel contentMiddlePanel = new JPanel();
    private final LongButton readyButton = new LongButton("READY",200,75, Color.decode("#21bf4c"));
    private final LongButton returnButton = new LongButton("RETURN",200,75, Color.decode("#eb4034"));
    private final LongButton switchButton = new LongButton("Rotate",100,75, Color.yellow);
    private Checkerboard checkerboard ;
    private Option option;
    private int customShipNumber;
    private String alignment = "horizontal";
    private ShipDeck shipdeck = new ShipDeck();
    private Sfx sfx = new Sfx();
    int indexForShip = 0;

    public PiecePlacement(Option option) {
        this.option = option;
        this.checkerboard = new Checkerboard(option.getCheckBoardSize(),option.getCheckBoardSize(),Form.Square,Color.decode("#2660d4"));
        initComponents();
    }

    public void initComponents(){

        this.setLayout(new BorderLayout());

        this.add(spacingPanelRight,BorderLayout.LINE_START);
        this.add(spacingPanelLeft,BorderLayout.LINE_END);
        this.add(spacingPanelNorth,BorderLayout.PAGE_START);
        this.add(contentMiddlePanel,BorderLayout.CENTER);

        contentMiddlePanel.setLayout(new FlowLayout());
        contentMiddlePanel.setBackground(Color.DARK_GRAY);


        contentMiddlePanel.add(checkerboard);
        if(option.getCheckBoardSize() >= 11){
            checkerboard.resizeCheckboard(40);
        }

        JPanel endPanel = new JPanel();
        endPanel.setLayout(new FlowLayout());
        endPanel.setBackground(Color.darkGray);
        endPanel.add(returnButton);
        endPanel.add(readyButton);
        endPanel.add(switchButton);
        endPanel.add( new Spacing(0,140));
        this.add(endPanel,BorderLayout.PAGE_END);

        readyButton.setFocusPainted(false);
        returnButton.setFocusPainted(false);
        switchButton.setFocusPainted(false);
        readyButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        returnButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        switchButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        readyButton.setForeground(Color.decode("#e4ede6"));
        returnButton.setForeground(Color.decode("#e4ede6"));


        returnButton.addActionListener(this);
        returnButton.setActionCommand("Return");

        switchButton.addActionListener(this);
        switchButton.setActionCommand("switch");


        readyButton.addActionListener(this);
        readyButton.setActionCommand("validate");

        for(int i = 0 ; i < checkerboard.getSquareList().size(); i++){
            SquareGRectangle SquareGPanel = (SquareGRectangle) checkerboard.getSquareList().get(i);
            SquareGPanel.addMouseListener(this);
            SquareGPanel.untakeSquare();
            requestFocus();
        }

        customShipNumber = option.getShipCustomListNumber();
    }

    public ShipDeck getPlayerShipDeck(){
        return shipdeck;
    }

    public boolean addPiecePlacementListener(PiecePlacementListener ecouteur) {
        return piecePlacementListener.add(ecouteur);
    }

    public void ReturnButtonPressed() {
        for (PiecePlacementListener ecouteurOption : piecePlacementListener) {
            shipdeck.clearDeck();
            ecouteurOption.ReturnButtonPressedFromPiecePlacement();
        }
    }
    public void validateButtonPressed(){
        for (PiecePlacementListener ecouteurOption : piecePlacementListener) {
            ecouteurOption.ValidateButtonPressedFromPiecePlacement();
        }
    }

    // ACTION LISTENER
    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        if(action.equals("Return")){
            ReturnButtonPressed();
            return;
        }
        if(action.equals("switch")){
            if(alignment.equals("horizontal")){
                alignment = "vertical";
            } else if (alignment.equals("vertical")) {
                alignment = "horizontal";
            }
        }
        if(action.equals("validate")){

            if(customShipNumber > 0){
                System.out.println("Ship not placed");
                return;
            }
            validateButtonPressed();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {

        SquareGRectangle square = (SquareGRectangle) e.getComponent();
        char squareX = (char) square.getCoordinate().getX();
        int squareY = square.getCoordinate().getY();


        if(customShipNumber <= 0){
            validateButtonPressed();
        }

        if(alignment.equals("horizontal")){

            if(customShipNumber > 0){

                int ShipLenght = option.getShipCustomList().get(indexForShip).getLenght();
                boolean freeToPlace = true;

                for(int i = 0 ; i < ShipLenght;i++ ){

                    Coordinate TestingCoordinate = new Coordinate((char) (squareX + i), squareY);

                    if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                        freeToPlace = false;

                    }
                }

                if(freeToPlace){
                    for(int i = 0 ; i < ShipLenght;i++ ){
                        Coordinate TestingCoordinate = new Coordinate((char) (squareX + i), squareY);
                        checkerboard.paintSquareByCoordonate(TestingCoordinate,Color.green);
                    }
                }
                else{
                    for(int i = 0 ; i < ShipLenght;i++ ){
                        Coordinate TestingCoordinate = new Coordinate((char) (squareX + i), squareY);
                        checkerboard.paintSquareByCoordonate(TestingCoordinate,Color.RED);
                    }
                }
                return;

            }
        }

        if(alignment.equals("vertical")){


            if(customShipNumber > 0){

                int ShipLenght = option.getShipCustomList().get(indexForShip).getLenght();
                boolean freeToPlace = true;

                for(int i = 0 ; i < ShipLenght;i++ ){

                    Coordinate TestingCoordinate = new Coordinate((char) (squareX ), squareY+ i);

                    if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                        freeToPlace = false;

                    }
                }

                if(freeToPlace){
                    for(int i = 0 ; i < ShipLenght;i++ ){
                        Coordinate TestingCoordinate = new Coordinate((char) (squareX), squareY+ i);
                        checkerboard.paintSquareByCoordonate(TestingCoordinate,Color.green);
                    }
                }
                else{
                    for(int i = 0 ; i < ShipLenght;i++ ){
                        Coordinate TestingCoordinate = new Coordinate((char) (squareX), squareY+ i);
                        checkerboard.paintSquareByCoordonate(TestingCoordinate,Color.RED);
                    }
                }
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {

        checkerboard.paintAllSquareIfNotTaken(Color.decode("#2660d4"));
        checkerboard.paintAllTakenSquare(Color.decode("#2660d4"));
        checkerboard.paintAllTakenSquareWithImage();
        repaint();
        revalidate();
    }
    @Override
    public void mousePressed(MouseEvent e) {
        SquareGRectangle square = (SquareGRectangle) e.getComponent();

        char squareX = (char) square.getCoordinate().getX();
        int squareY = square.getCoordinate().getY();

        if(customShipNumber <= 0){
            System.out.println("PIECE PLACEMENT ENDED");
        }

        if(alignment.equals("horizontal")){

            if(customShipNumber > 0){

                int ShipLenght = option.getShipCustomList().get(indexForShip).getLenght();
                boolean freeToPlace = true;
                LinkedList<Piece> tempPieceList = new LinkedList<>();
                LinkedList<Coordinate> tempCoordList = new LinkedList<>();

                for(int i = 0 ; i < ShipLenght;i++ ){

                    Coordinate TestingCoordinate = new Coordinate((char) (squareX + i), squareY);

                    if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                        freeToPlace = false;
                    }
                    else {
                        tempPieceList.add(new Piece(TestingCoordinate));
                        tempCoordList.add(TestingCoordinate);
                    }
                }

                if(freeToPlace){

                    option.getShipCustomList().get(indexForShip).AddPieceToShipCustom(tempPieceList);
                    shipdeck.addCustomShipToDeck(option.getShipCustomList().get(indexForShip));
                    checkerboard.takeSquareByCoordonateList(tempCoordList);
                    for(int i = 0 ; i < tempCoordList.size();i++){
                        checkerboard.paintImageSquareByCoordonate(tempCoordList.get(i));
                        repaint();
                        revalidate();
                    }
                    try {
                        sfx.playSfx("src/Component/Sound/SfxStock/Fleet.wav");
                    } catch (LineUnavailableException x) {
                        throw new RuntimeException(x);
                    }
                }
                else{
                    tempPieceList.clear();
                    tempCoordList.clear();
                    return;
                }

                indexForShip ++;
                customShipNumber--;
                System.out.println("Nombre de bateau à placer = "+customShipNumber);
                checkerboard.paintAllSquareIfNotTaken(Color.decode("#2660d4"));
                checkerboard.paintAllTakenSquare(Color.decode("#2660d4"));
                checkerboard.paintAllTakenSquareWithImage();
                return;
            }
        }

        if(alignment.equals("vertical")){

            if(customShipNumber > 0){

                int ShipLenght = option.getShipCustomList().get(indexForShip).getLenght();
                boolean freeToPlace = true;
                LinkedList<Piece> tempPieceList = new LinkedList<>();
                LinkedList<Coordinate> tempCoordList = new LinkedList<>();

                for(int i = 0 ; i < ShipLenght;i++ ){

                    Coordinate TestingCoordinate = new Coordinate((char) (squareX), squareY+ i);

                    if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                        freeToPlace = false;
                    }
                    else {
                        tempPieceList.add(new Piece(TestingCoordinate));
                        tempCoordList.add(TestingCoordinate);
                    }
                }

                if(freeToPlace){

                    option.getShipCustomList().get(indexForShip).AddPieceToShipCustom(tempPieceList);
                    shipdeck.addCustomShipToDeck(option.getShipCustomList().get(indexForShip));
                    checkerboard.takeSquareByCoordonateList(tempCoordList);

                    for(int i = 0 ; i < tempCoordList.size();i++){
                        checkerboard.paintImageSquareByCoordonate(tempCoordList.get(i));
                        repaint();
                        revalidate();
                    }
                    try {
                        sfx.playSfx("src/Component/Sound/SfxStock/Fleet.wav");
                    } catch (LineUnavailableException x) {
                        throw new RuntimeException(x);
                    }
                }
                else{
                    tempPieceList.clear();
                    tempCoordList.clear();
                    return;
                }

                indexForShip ++;
                customShipNumber--;
                checkerboard.paintAllSquareIfNotTaken(Color.decode("#2660d4"));
                checkerboard.paintAllTakenSquare(Color.decode("#2660d4"));
                checkerboard.paintAllTakenSquareWithImage();
            }
        }
    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }

}


