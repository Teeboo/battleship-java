package Component.Panel;

import Component.Elements.Button.LongButton;
import Component.Elements.Checkerboard;
import Component.Elements.Coordinate.Coordinate;
import Component.Elements.Deck.ShipDeck;
import Component.Elements.Options.Option;
import Component.Elements.Piece.Piece;
import Component.Elements.Piece.ShipCustom;
import Component.Elements.Square.Form;
import Component.Elements.Square.SquareGRectangle;
import Component.IA.Ia;
import Component.Listener.GameplayListener;
import Component.Rules.BattleshipRules;
import Component.Sound.Sfx;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Gameplay extends JPanel implements ActionListener, MouseListener {

    private BattleshipRules gameRule = new BattleshipRules();
    private Set<GameplayListener> gameplayListeners = new HashSet<>();
    private boolean gameFinished = false;
    private final JPanel leftGameplayPanel = new JPanel();
    private final JPanel rightGameplayPanel = new JPanel();
    private final Spacing spacingPanelNorth = new Spacing(0,40);
    private final JPanel footerPanel = new JPanel();
    private final JPanel CenterPanel = new JPanel();
    private JTextField CoordinateTextField = new JTextField();
    private JTextField DestroyedShipRecapTextField = new JTextField();
    private JTextField ActiveShipRecapTextField = new JTextField();
    private final LongButton returnButton = new LongButton("RETURN",200,75, Color.decode("#eb4034"));
    private final LongButton endingButton = new LongButton("",250,75,Color.decode("#21bf4c"));
    private Option option;
    private Checkerboard playerCheckerboard;
    private Checkerboard iaCheckerboard;
    private ShipDeck playerDeck;
    private ShipDeck iaDeck = new ShipDeck();
    private JPanel ElemebtPanel = new JPanel();
    private Ia ia ;

    private Sfx sfx = new Sfx();
    public Gameplay(Option option,ShipDeck playerDeck) {
        this.option = option;
        this.playerDeck = playerDeck;

        playerCheckerboard = new Checkerboard(option.getCheckBoardSize(),option.getCheckBoardSize(), Form.Square, Color.decode("#2660d4"));
        iaCheckerboard = new Checkerboard(option.getCheckBoardSize(),option.getCheckBoardSize(), Form.Square, Color.decode("#2660d4"));
        try {
            iaDeck.generateIaDeck(iaCheckerboard,option);
        } catch (Exception e) {
            ReturnButtonPressed();
            throw new RuntimeException(e);
        }

        if(option.getCheckBoardSize() >= 11){
            playerCheckerboard.resizeCheckboard(38);
            iaCheckerboard.resizeCheckboard(38);
        }
        initComponents();
        addListenerToEnemyCheckboard();
    }
    public void initComponents(){

        this.setLayout(new BorderLayout());
        this.setBackground(Color.DARK_GRAY);
        leftGameplayPanel.setBackground(Color.DARK_GRAY);
        rightGameplayPanel.setBackground(Color.DARK_GRAY);

        leftGameplayPanel.setLayout(new FlowLayout());
        rightGameplayPanel.setLayout(new FlowLayout());

        leftGameplayPanel.add(new Spacing(35,0));
        leftGameplayPanel.add(playerCheckerboard);
        rightGameplayPanel.add(iaCheckerboard);
        rightGameplayPanel.add(new Spacing(35,0));

        playerDeck.applyDeckToCheckerBoard(playerCheckerboard);

        ElemebtPanel.setBackground(Color.DARK_GRAY);
        ElemebtPanel.setLayout(new GridLayout(6,1));

        CenterPanel.setLayout(new FlowLayout());
        CenterPanel.setBackground(Color.DARK_GRAY);
        CenterPanel.add(ElemebtPanel);

        ElemebtPanel.add(new Spacing(0,50));
        CoordinateTextField.setPreferredSize(new Dimension(100,50));
        CoordinateTextField.setMaximumSize((new Dimension(100,50)));
        CoordinateTextField.setFocusable(false);
        CoordinateTextField.setEditable(false);
        CoordinateTextField.setHorizontalAlignment(JTextField.CENTER);
        CoordinateTextField.setFont(new Font("Tahoma", Font.PLAIN, 14));
        ElemebtPanel.add(CoordinateTextField);
        ElemebtPanel.add(new Spacing(0,50));

        DestroyedShipRecapTextField.setPreferredSize(new Dimension(250,50));
        DestroyedShipRecapTextField.setFocusable(false);
        DestroyedShipRecapTextField.setEditable(false);
        DestroyedShipRecapTextField.setHorizontalAlignment(JTextField.CENTER);
        DestroyedShipRecapTextField.setFont(new Font("Helvetica", Font.BOLD, 14));
        DestroyedShipRecapTextField.setText("Battleships destroyed : "+iaDeck.getNumberOfDestroyedShip());
        DestroyedShipRecapTextField.setBackground(Color.decode("#2660d4"));
        DestroyedShipRecapTextField.setForeground(Color.decode("#e4ede6"));
        ElemebtPanel.add(DestroyedShipRecapTextField);
        ElemebtPanel.add(new Spacing(0,50));

        ActiveShipRecapTextField.setPreferredSize(new Dimension(250,50));
        ActiveShipRecapTextField.setFocusable(false);
        ActiveShipRecapTextField.setEditable(false);
        ActiveShipRecapTextField.setHorizontalAlignment(JTextField.CENTER);
        ActiveShipRecapTextField.setFont(new Font("Helvetica", Font.BOLD, 14));
        ActiveShipRecapTextField.setText("Battleships left : "+iaDeck.getNumberOfActiveShip());
        ActiveShipRecapTextField.setBackground(Color.decode("#2660d4"));
        ActiveShipRecapTextField.setForeground(Color.decode("#e4ede6"));
        ElemebtPanel.add(ActiveShipRecapTextField);


        footerPanel.setLayout(new FlowLayout());
        footerPanel.add(returnButton);
        returnButton.setFocusPainted(false);
        returnButton.addActionListener(this);
        returnButton.setActionCommand("return");
        returnButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        returnButton.setForeground(Color.decode("#e4ede6"));
        footerPanel.add(new Spacing(0,150));
        footerPanel.setBackground(Color.DARK_GRAY);

        this.add(leftGameplayPanel,BorderLayout.LINE_START);
        this.add(rightGameplayPanel,BorderLayout.LINE_END);
        this.add(spacingPanelNorth,BorderLayout.PAGE_START);
        this.add(CenterPanel,BorderLayout.CENTER);
        this.add(footerPanel,BorderLayout.PAGE_END);

        playerDeck.getAllPieceFromDeck();
        iaDeck.getAllPieceFromDeck();
        ia = new Ia(playerCheckerboard,option,playerDeck);


    }

    public void refreshDestroyedBattleshipNumber(){
        DestroyedShipRecapTextField.setText("Battleships destroyed : "+iaDeck.getNumberOfDestroyedShip());
        ActiveShipRecapTextField.setText("Battleships left : "+iaDeck.getNumberOfActiveShip());
    }
    public void refreshDestroyedBattleshipImage(){
        for(int i = 0 ; i < iaDeck.getCustomsShipList().size();i++){
            if(iaDeck.checkIfShipIsDestroyed(iaDeck.getCustomsShipList().get(i))){
                LinkedList<Piece> PieceList= iaDeck.getCustomsShipList().get(i).getPieceList();
                System.out.println(PieceList);
                for(int x = 0 ; x < PieceList.size();x++){
                    Coordinate tempCoord = PieceList.get(x).getCoordinate();
                    iaCheckerboard.paintFireImageSquareByCoordonate(tempCoord);
                }
            }

        }
    }
    public void addListenerToEnemyCheckboard(){

        for(int i = 0 ; i < iaCheckerboard.getSquareList().size(); i++){
            SquareGRectangle SquareGPanel = (SquareGRectangle) iaCheckerboard.getSquareList().get(i);
            SquareGPanel.addMouseListener(this);
        }
    }

    public boolean addPiecePlacementListener(GameplayListener ecouteur) {
        return gameplayListeners.add(ecouteur);
    }

    public void ReturnButtonPressed() {
        for (GameplayListener ecouteurGameplay : gameplayListeners) {
            for(int i = 0 ; i < this.playerDeck.getCustomsShipList().size();i++){
            }
            ecouteurGameplay.ReturnButtonPressedFromGameplay();
        }
    }
    public void FinishGame(String winnerName){

        gameFinished = true;
        CenterPanel.remove(ElemebtPanel);
        JPanel EndingGamePanel = new JPanel();
        EndingGamePanel.setLayout(new FlowLayout());
        EndingGamePanel.setBackground(Color.DARK_GRAY);
        endingButton.setFocusable(false);
        endingButton.setForeground(Color.decode("#e4ede6"));
        endingButton.setFont(new Font("Helvetica", Font.BOLD, 14));

        CenterPanel.add(EndingGamePanel);
        returnButton.setBackground(Color.decode("#21bf4c"));

        for (GameplayListener ecouteurGameplay : gameplayListeners) {
            ecouteurGameplay.GameFinished(winnerName);
        }
        EndingGamePanel.add(endingButton);
        EndingGamePanel.add( new Spacing(0,240));

        if(winnerName.equals("Player")){
            endingButton.setText("VICTORY");
        } else {
            endingButton.setText("DEFEAT");
            endingButton.setBackground(Color.red);
            iaCheckerboard.paintAllTakenSquare(Color.DARK_GRAY);
        }
        repaint();
        revalidate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        if(action.equals("return")){

            playerDeck.clearDeck();
            iaDeck.clearDeck();
            playerCheckerboard.clearCheckerBoard();
            iaCheckerboard.clearCheckerBoard();
            ReturnButtonPressed();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {


    }
    @Override
    public void mousePressed(MouseEvent e) {
        if(gameFinished){
            return;
        }

        refreshDestroyedBattleshipNumber();
        SquareGRectangle square = (SquareGRectangle) e.getComponent();
        Coordinate squareCoord = square.getCoordinate();

        if(!square.isUsable()){
            return;
        }
        if(square.isTaken()){

            square.disableButton();
            Piece touchedPiece = iaDeck.getPieceFromCoord(squareCoord);
            touchedPiece.setDestroyed(true);
            boolean destroyed = false;

            for(int i = 0 ; i < iaDeck.getCustomsShipList().size();i++){
                ShipCustom tempShip = iaDeck.getCustomsShipList().get(i);
                LinkedList<Piece> pieceList = tempShip.getPieceList();

                for(int x = 0 ; x < pieceList.size();x++){
                    if (pieceList.get(x).getCoordinate().equals(squareCoord)){
                        if(playerDeck.checkIfShipIsDestroyed(tempShip)){
                            System.out.println("SHIP DESTROYED");
                            tempShip.setDestroyed(true);
                            refreshDestroyedBattleshipNumber();
                            refreshDestroyedBattleshipImage();

                            if (option.getCheckBoardSize() >= 11) {
                                square.applyShipFireImage("Lower");

                            }else {
                                square.applyShipFireImage("Normal");
                            }

                            destroyed = true;
                            try {
                                sfx.playSfx("src/Component/Sound/SfxStock/Destroy.wav");
                            } catch (LineUnavailableException y) {
                                throw new RuntimeException(y);
                            }
                        }
                    }
                }
            }
            if(!destroyed){

                if(option.getCheckBoardSize() >= 11){
                    square.applyShipImage("Lower");
                }else{
                    square.applyShipImage("Normal");
                }

                try {
                    sfx.playSfx("src/Component/Sound/SfxStock/HitShip.wav");
                } catch (LineUnavailableException y) {
                    throw new RuntimeException(y);
                }
            }
            repaint();
            revalidate();

            if(gameRule.CheckVictory(iaDeck.getPieceList())){
                FinishGame("Player");
                return;
            }
        }
        else{
            square.disableButton();
            if (option.getCheckBoardSize() >= 11) {
                square.applyMissImage("Lower");

            }else {
                square.applyMissImage("Normal");
            }
            repaint();
            revalidate();
            refreshDestroyedBattleshipNumber();

        }
        ia.PlayTurn();

        if(gameRule.CheckVictory(playerDeck.getPieceList())){
            FinishGame("IA");
        }
    }
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        SquareGRectangle square = (SquareGRectangle) e.getComponent();
        CoordinateTextField.setText(""+square.getCoordinate().getY()+" - "+(char)square.getCoordinate().getX());
    }
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
