package Component.Panel;

import Component.Elements.Button.LongButton;
import Component.Listener.EndingPanelListener;
import Component.Listener.PiecePlacementListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

public class EndPanel extends JPanel implements ActionListener {

    private Set<EndingPanelListener> endingPanelListeners = new HashSet<>();
    private final String winnerName;
    private final Spacing spacingPanelRight = new Spacing(400,400);
    private final Spacing spacingPanelLeft = new Spacing(400,400);
    private final Spacing spacingPanelNorth = new Spacing(0,300);
    private final JPanel contentMiddlePanel = new JPanel();
    private final LongButton endingButton = new LongButton("",400,75,Color.decode("#21bf4c"));
    private final LongButton returnButton = new LongButton("RETURN",400,75, Color.decode("#eb4034"));


    public EndPanel(String winnerName) {
        this.winnerName = winnerName;
        initComponents();
    }

    public void initComponents(){

        this.setLayout(new BorderLayout());
        this.add(spacingPanelRight,BorderLayout.LINE_START);
        this.add(spacingPanelLeft,BorderLayout.LINE_END);
        this.add(spacingPanelNorth,BorderLayout.PAGE_START);
        this.add(contentMiddlePanel,BorderLayout.CENTER);

        endingButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        returnButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        endingButton.setForeground(Color.decode("#e4ede6"));
        returnButton.setForeground(Color.decode("#e4ede6"));

        contentMiddlePanel.setLayout(new FlowLayout());
        contentMiddlePanel.setBackground(Color.DARK_GRAY);
        contentMiddlePanel.add(endingButton);
        contentMiddlePanel.add( new Spacing(0,150));
        contentMiddlePanel.add(returnButton);

        if(winnerName.equals("Player")){
            endingButton.setText("YOU WIN");
        } else if (winnerName.equals("IA")) {
            endingButton.setText("YOU LOOSE");
            endingButton.setBackground(Color.decode("#eb4034"));
        }
        endingButton.setFocusPainted(false);
        returnButton.setFocusPainted(false);
        returnButton.addActionListener(this);
        returnButton.setActionCommand("Return");
    }

    public boolean addEndingPanelListener(EndingPanelListener ecouteur) {
        return endingPanelListeners.add(ecouteur);
    }

    public void ReturnButtonPressed() {
        for (EndingPanelListener ecouteurOption : endingPanelListeners) {
            ecouteurOption.ReturnPressedFromEndingPanel();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String action = e.getActionCommand();

        if(action.equals("Return")){
            ReturnButtonPressed();

        }
    }
}
