package Component.Panel;

import Component.Elements.Button.LongButton;
import Component.Listener.MenuListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

public class Menu extends JPanel implements ActionListener {


    private final Spacing spacingPanelRight = new Spacing(400,400);
    private final Spacing spacingPanelLeft = new Spacing(400,400);
    private final Spacing spacingPanelNorth = new Spacing(0,240);
    private final JPanel contentMiddlePanel = new JPanel();
    private final LongButton playButton = new LongButton("PLAY",400,75,Color.decode("#2660d4"));
    private final LongButton optionButton = new LongButton("OPTIONS",400,75,Color.decode("#2660d4"));
    private final LongButton closeButton = new LongButton("Quit",400,75,Color.decode("#eb4034"));

    private final JPanel EndingPanel = new JPanel();

    public Menu() {
        initComponents();
    }

    public void initComponents(){

        this.setLayout(new BorderLayout());

        this.add(spacingPanelRight,BorderLayout.LINE_START);
        this.add(spacingPanelLeft,BorderLayout.LINE_END);
        this.add(spacingPanelNorth,BorderLayout.PAGE_START);
        this.add(contentMiddlePanel,BorderLayout.CENTER);
        this.add(EndingPanel,BorderLayout.PAGE_END);

        EndingPanel.setLayout(new FlowLayout());
        EndingPanel.setBackground(Color.DARK_GRAY);
        EndingPanel.add(closeButton);
        closeButton.setFocusPainted(false);
        closeButton.addActionListener(this);
        closeButton.setActionCommand("Quit");
        closeButton.setForeground(Color.decode("#e4ede6"));
        closeButton.setFont(new Font("Helvetica", Font.BOLD, 14));
        EndingPanel.add( new Spacing(0,240));

        contentMiddlePanel.setLayout(new FlowLayout());
        contentMiddlePanel.setBackground(Color.DARK_GRAY);
        contentMiddlePanel.add(playButton);
        playButton.addActionListener(this);
        playButton.setActionCommand("PLAY");
        playButton.setFocusPainted(false);
        playButton.setForeground(Color.decode("#e4ede6"));
        playButton.setFont(new Font("Helvetica", Font.BOLD, 14));

        contentMiddlePanel.add(new Spacing(0,150));
        contentMiddlePanel.add(optionButton);
        optionButton.addActionListener(this);
        optionButton.setActionCommand("OPTION");
        optionButton.setFocusPainted(false);
        optionButton.setForeground(Color.decode("#e4ede6"));
        optionButton.setFont(new Font("Helvetica", Font.BOLD, 14));
    }

    // LISTENER SET
    private Set<MenuListener> menuListener = new HashSet<>();

    public boolean addMenuListener(MenuListener ecouteur) {
        return menuListener.add(ecouteur);
    }

    public void CallListenerPlayButtonPressed() {
        for (MenuListener ecouteurMenu : menuListener) {
            ecouteurMenu.PlayButtonPressed();
        }
    }

    public void CallListenerOptionButtonPressed(){
        for (MenuListener ecouteurMenu : menuListener) {
            ecouteurMenu.OptionButtonPressed();
        }
    }

    public void QuitButtonPressed(){
        for (MenuListener ecouteurMenu : menuListener) {
            ecouteurMenu.QuitButtonPressed();
        }
    }

    // ACTION LISTENER

    @Override
    public void actionPerformed(ActionEvent e) {

        String action = e.getActionCommand();

        if(action.equals("PLAY")){
            CallListenerPlayButtonPressed();
            return;
        }
        if (action.equals("OPTION")){
            CallListenerOptionButtonPressed();
            return;
        }
        if(action.equals("Quit")){
            QuitButtonPressed();
        }

    }
}
