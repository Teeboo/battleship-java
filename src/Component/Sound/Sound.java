package Component.Sound;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.*;

public class Sound {


    public Clip clip;
    ClassLoader cl = this.getClass().getClassLoader();

    public void playMusic(String filePath) throws LineUnavailableException {

        InputStream music;
        File musicPath = new File(filePath);

        try {
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(musicPath);
            clip = AudioSystem.getClip();
            clip.open(audioInput);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);

        } catch (UnsupportedAudioFileException e) {
            throw new RuntimeException(e);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void stopMusic(){

    clip.stop();

    }



}
