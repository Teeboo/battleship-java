package Component.Elements.Deck;

import Component.Elements.Checkerboard;
import Component.Elements.Coordinate.Coordinate;
import Component.Elements.Options.Option;
import Component.Elements.Piece.Piece;

import Component.Elements.Piece.ShipCustom;
import Component.Elements.Square.SquareGRectangle;


import java.awt.*;
import java.util.LinkedList;
import java.util.Random;

public class ShipDeck {

    private String playername;
    private final LinkedList<ShipCustom> customsShipList = new  LinkedList<>();
    private final LinkedList<Piece> pieceList = new LinkedList<Piece>();

    // CONSTRUCTOR

    public ShipDeck(String playername) {
        this.playername = playername;
    }
    public ShipDeck() {
        this.playername ="Default name";
    }

    // GETTER SETTER


    public LinkedList<ShipCustom> getCustomsShipList() {
        return customsShipList;
    }

    public LinkedList<Piece> getPieceList() {
        return pieceList;
    }

    // FUNCTION

    public void clearDeck(){
        customsShipList.clear();
        pieceList.clear();
    }


    public int getNumberOfDestroyedShip(){
        int result = 0;
        for(int i = 0 ; i < customsShipList.size();i++){
            if(customsShipList.get(i).isDestroyed()){
                result++;
            }
        }
        return result;
    }
    public int getNumberOfActiveShip(){
        int result = 0;
        for(int i = 0 ; i < customsShipList.size();i++){
            if(!customsShipList.get(i).isDestroyed()){
                result++;
            }
        }
        return result;
    }

    public boolean checkIfShipIsDestroyed(ShipCustom customship){
        boolean result = true;
        for(int i = 0 ; i < customship.getPieceList().size();i++){
            Piece tempPiece = (Piece) customship.getPieceList().get(i);
            if(!tempPiece.isDestroyed()){
                result = false;
            }
        }
        return result;
    }

    public void getAllPieceFromDeck(){
        for(int i = 0 ; i < customsShipList.size();i++){
            ShipCustom customShip = customsShipList.get(i);
            LinkedList<Piece> pieceFromCustomShip = customShip.getPieceList();

            for(int x = 0 ; x <pieceFromCustomShip.size();x++ ){
                pieceList.add(pieceFromCustomShip.get(x));

            }
        }
    }
    public void getAllPieceFromDeck(LinkedList<ShipCustom> shipList){

        for(int i = 0 ; i < customsShipList.size();i++){
            ShipCustom customShip = customsShipList.get(i);
            LinkedList<Piece> pieceFromCustomShip = customShip.getPieceList();

            for(int x = 0 ; x <pieceFromCustomShip.size();x++ ){
                pieceList.add(pieceFromCustomShip.get(x));
            }
        }
    }

    public Piece getPieceFromCoord(Coordinate coordinate){

        Piece pieceNull = new Piece();
        for(int i = 0 ; i < pieceList.size();i++){
            Piece tempPiece = pieceList.get(i);
            if(tempPiece.getCoordinate().equals(coordinate)){
                return tempPiece;
            }
        }
        return pieceNull;
    }
    public void showAllPieceFromDeckDestroyed(){
        for(int i = 0 ; i < pieceList.size();i++){
            Piece piece = pieceList.get(i);
            if(piece.isDestroyed()){
                System.out.println("Piece numéro "+i+" = "+piece);
            }
        }
    }
    public void showAllPieceFromDeckNotDestroyed(){
        for(int i = 0 ; i < pieceList.size();i++){
            Piece piece = pieceList.get(i);
            if(!piece.isDestroyed()){
                System.out.println("Piece numéro "+i+" = "+piece +" : EN VIE");
            }

        }
    }
    public void addCustomShipToDeck(ShipCustom shipCustom){
        customsShipList.add(shipCustom);
    }
    public void applyDeckToCheckerBoard(Checkerboard checkerboard){

        for(int i = 0 ; i < customsShipList.size(); i++){
            ShipCustom tempship = getCustomsShipList().get(i);
            LinkedList NavirePieceList = tempship.getPieceList();

            for(int x = 0; x < NavirePieceList.size(); x++){
                Piece tempCoord = (Piece) NavirePieceList.get(x);
                checkerboard.paintImageSquareByCoordonate(tempCoord.getCoordinate());
                checkerboard.takeSquareByCoordonate(tempCoord.getCoordinate());
            }
        }
    }

    public ShipDeck generateIaDeck(Checkerboard checkerboard, Option option) throws Exception {

        ShipDeck iaDeck = this;
        LinkedList squareList = checkerboard.getSquareList();
        LinkedList shipList = option.getShipCustomList();
        Random randomDirection = new Random();

        for (int x = 0; x < shipList.size() ; x++){

            LinkedList<Coordinate> availaibleCoordList = new LinkedList<>();
            int intRandomDirection = randomDirection.nextInt(2);
            ShipCustom tempShipCustom = (ShipCustom) shipList.get(x);
            System.out.println(tempShipCustom);
            int ShipLenght = option.getShipCustomList().get(x).getLenght();

            if (intRandomDirection == 0) {

                for(int i = 0; i < squareList.size(); i++){

                    boolean availaible = true;
                    SquareGRectangle tempsquare = (SquareGRectangle) squareList.get(i);
                    char coordX = (char) tempsquare.getCoordinate().getX();
                    int coordY = tempsquare.getCoordinate().getY();

                    for(int y = 0 ; y < ShipLenght;y++ ){

                        Coordinate TestingCoordinate = new Coordinate((char) (coordX + y), coordY);

                        if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                            availaible = false;
                        }
                    }

                    if(availaible){
                        availaibleCoordList.add(tempsquare.getCoordinate());
                    }
                }
                if(availaibleCoordList.isEmpty()){
                    System.out.println("PROBLEM");
                    throw new Exception("Not enought space for Batlleship");
                }

                Random random = new Random();
                int intRandom = random.nextInt(availaibleCoordList.size());
                Coordinate ChoicedCoordinate = availaibleCoordList.get(intRandom);
                ShipCustom customNewShip = new ShipCustom(ShipLenght);
                LinkedList<Piece> pieceFromGeneratedShip = new LinkedList<>();

                iaDeck.addCustomShipToDeck(customNewShip);

                for(int y = 0 ; y < ShipLenght;y++ ){
                    Coordinate TestingCoordinate = new Coordinate((char) (ChoicedCoordinate.getX() + y), ChoicedCoordinate.getY());
                    pieceFromGeneratedShip.add(new Piece(TestingCoordinate));
                    //checkerboard.paintSquareByCoordonate(TestingCoordinate,Color.green);
                    checkerboard.takeSquareByCoordonate(new Coordinate((char) (ChoicedCoordinate.getX() + y), ChoicedCoordinate.getY()));
                }
                customNewShip.AddPieceToShipCustom(pieceFromGeneratedShip);
            }

            if (intRandomDirection == 1) {

                for(int i = 0; i < squareList.size(); i++){

                    boolean availaible = true;
                    SquareGRectangle tempsquare = (SquareGRectangle) squareList.get(i);
                    char coordX = (char) tempsquare.getCoordinate().getX();
                    int coordY = tempsquare.getCoordinate().getY();

                    for(int y = 0 ; y < ShipLenght;y++ ){

                        Coordinate TestingCoordinate = new Coordinate((char) (coordX), coordY+ y);

                        if(!checkerboard.CheckIfSquareExist(TestingCoordinate)){
                            availaible = false;
                        }
                    }

                    if(availaible){
                        availaibleCoordList.add(tempsquare.getCoordinate());
                    }
                }
                if(availaibleCoordList.isEmpty()){
                    System.out.println("PROBLEM");
                    throw new Exception("Not enought space for Batlleship");
                }

                Random random = new Random();
                int intRandom = random.nextInt(availaibleCoordList.size());
                Coordinate ChoicedCoordinate = availaibleCoordList.get(intRandom);
                ShipCustom customNewShip = new ShipCustom(ShipLenght);
                LinkedList<Piece> pieceFromGeneratedShip = new LinkedList<>();

                iaDeck.addCustomShipToDeck(customNewShip);

                for(int y = 0 ; y < ShipLenght;y++ ){
                    Coordinate TestingCoordinate = new Coordinate((char) (ChoicedCoordinate.getX()), ChoicedCoordinate.getY()+ y);
                    pieceFromGeneratedShip.add(new Piece(TestingCoordinate));
                    checkerboard.takeSquareByCoordonate(new Coordinate((char) (ChoicedCoordinate.getX()), ChoicedCoordinate.getY()+ y));

                }
                customNewShip.AddPieceToShipCustom(pieceFromGeneratedShip);

            }

        }
        return iaDeck;
    }

    @Override
    public String toString() {
        return "ShipDeck{" +
                "playername='" + playername + '\'' +
                ", customsShipList=" + customsShipList +
                ", pieceList=" + pieceList +
                '}';
    }
}
