package Component.Elements.Button;

import javax.swing.*;
import java.awt.*;

public class LongButton extends JButton {

    private int width ;
    private int length;
    private Color color;



    public LongButton(String buttonText,int width, int length) {

        this.setPreferredSize(new Dimension(width, length));
        this.setMaximumSize(new Dimension(width, length));
        this.setText(buttonText);
    }

    public LongButton(String buttonText,int width, int length,Color color) {

        this.setPreferredSize(new Dimension(width, length));
        this.setMaximumSize(new Dimension(width, length));
        this.setText(buttonText);
        this.setBackground(color);

    }


}
