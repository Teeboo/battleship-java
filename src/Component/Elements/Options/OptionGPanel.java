package Component.Elements.Options;

import Component.Elements.Button.LongButton;
import Component.Listener.OptionListener;
import Component.Panel.Spacing;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

public class OptionGPanel extends JPanel implements ActionListener {

    private Option option;
    private final Spacing spacingPanelRight = new Spacing(400,10);
    private final Spacing spacingPanelLeft = new Spacing(400,10);
    private final Spacing spacingPanelNorth = new Spacing(0,240);
    private final JPanel EndingPanel = new JPanel();
    private final JPanel contentMiddlePanel = new JPanel();
    private final JTextField sizeFieldText = new JTextField();
    private final LongButton AddButton = new LongButton("+",75,75,Color.decode("#2660d4"));
    private final LongButton SubButton = new LongButton("-",75,75,Color.decode("#2660d4"));
    private final LongButton validateButton = new LongButton("Ok",400,75,Color.decode("#21bf4c"));
    private final JTextField ship3Piece = new JTextField();
    private final LongButton AddButtonShip3Piece = new LongButton("+",75,75,Color.decode("#2660d4"));
    private final LongButton SubButtonShip3Piece = new LongButton("-",75,75,Color.decode("#2660d4"));
    private final JTextField ship2Piece = new JTextField();
    private final LongButton AddButtonShip2Piece = new LongButton("+",75,75,Color.decode("#2660d4"));
    private final LongButton SubButtonShip2Piece = new LongButton("-",75,75,Color.decode("#2660d4"));
    private final JTextField ship4Piece = new JTextField();
    private final LongButton AddButtonShip4Piece = new LongButton("+",75,75,Color.decode("#2660d4"));
    private final LongButton SubButtonShip4Piece = new LongButton("-",75,75,Color.decode("#2660d4"));

    public OptionGPanel(Option option) {
        this.option = option;
        initComponents();
    }

    public void initComponents(){
        this.setLayout(new BorderLayout());

        this.add(spacingPanelRight,BorderLayout.LINE_START);
        this.add(spacingPanelLeft,BorderLayout.LINE_END);
        this.add(spacingPanelNorth,BorderLayout.PAGE_START);
        this.add(contentMiddlePanel,BorderLayout.CENTER);
        this.add(EndingPanel,BorderLayout.PAGE_END);

        EndingPanel.setLayout(new FlowLayout());
        EndingPanel.setBackground(Color.DARK_GRAY);
        EndingPanel.add(validateButton);
        EndingPanel.add( new Spacing(0,240));
        validateButton.addActionListener(this);
        validateButton.setActionCommand("Ok");
        validateButton.setForeground(Color.decode("#e4ede6"));
        validateButton.setFocusable(false);
        validateButton.setFont(new Font("Helvetica", Font.BOLD, 14));

        JPanel sizePanel = new JPanel();
        sizePanel.setLayout(new FlowLayout());
        sizePanel.setBackground(Color.darkGray);

        contentMiddlePanel.setLayout(new GridLayout(4,1));
        contentMiddlePanel.setBackground(Color.DARK_GRAY);

        sizePanel.add( new Spacing(0,5));
        sizePanel.add(sizeFieldText,BorderLayout.CENTER);
        sizePanel.add( new Spacing(0,5));

        sizeFieldText.setPreferredSize(new Dimension(400,75));
        sizeFieldText.setMaximumSize(new Dimension(400,75));
        sizeFieldText.setEditable(false);
        sizeFieldText.setBackground(Color.decode("#2660d4"));
        sizeFieldText.setFocusable(false);
        sizeFieldText.setHorizontalAlignment(JTextField.CENTER);
        sizeFieldText.setFont(new Font("Helvetica", Font.BOLD, 14));
        sizeFieldText.setText("Size of the checkboard ="+option.getCheckBoardSize());
        sizeFieldText.setForeground(Color.decode("#e4ede6"));

        ship3Piece.setPreferredSize(new Dimension(400,75));
        ship3Piece.setMaximumSize(new Dimension(400,75));
        ship3Piece.setEditable(false);
        ship3Piece.setBackground(Color.decode("#2660d4"));
        ship3Piece.setFocusable(false);
        ship3Piece.setHorizontalAlignment(JTextField.CENTER);
        ship3Piece.setFont(new Font("Helvetica", Font.BOLD, 14));
        ship3Piece.setText("Navire = "+option.getNumberOfShipByLenght(3));
        ship3Piece.setForeground(Color.decode("#e4ede6"));

        ship2Piece.setPreferredSize(new Dimension(400,75));
        ship2Piece.setMaximumSize(new Dimension(400,75));
        ship2Piece.setEditable(false);
        ship2Piece.setBackground(Color.decode("#2660d4"));
        ship2Piece.setFocusable(false);
        ship2Piece.setHorizontalAlignment(JTextField.CENTER);
        ship2Piece.setFont(new Font("Helvetica", Font.BOLD, 14));
        ship2Piece.setText("Submarine = "+option.getNumberOfShipByLenght(2));
        ship2Piece.setForeground(Color.decode("#e4ede6"));

        ship4Piece.setPreferredSize(new Dimension(400,75));
        ship4Piece.setMaximumSize(new Dimension(400,75));
        ship4Piece.setEditable(false);
        ship4Piece.setBackground(Color.decode("#2660d4"));
        ship4Piece.setFocusable(false);
        ship4Piece.setHorizontalAlignment(JTextField.CENTER);
        ship4Piece.setFont(new Font("Helvetica", Font.BOLD, 14));
        ship4Piece.setText("Destroyer = "+option.getNumberOfShipByLenght(4));
        ship4Piece.setForeground(Color.decode("#e4ede6"));

        sizePanel.add(AddButton,BorderLayout.CENTER);
        AddButton.setFocusPainted(false);
        AddButton.addActionListener(this);
        AddButton.setActionCommand("+");
        AddButton.setForeground(Color.decode("#e4ede6"));


        sizePanel.add(SubButton,BorderLayout.CENTER);
        SubButton.setFocusPainted(false);
        SubButton.addActionListener(this);
        SubButton.setActionCommand("-");
        SubButton.setForeground(Color.decode("#e4ede6"));
        contentMiddlePanel.add(sizePanel);


        JPanel ship4PiecePanel = new JPanel();
        ship4PiecePanel.setLayout(new FlowLayout());
        ship4PiecePanel.setBackground(Color.darkGray);
        ship4PiecePanel.add( new Spacing(0,5));
        ship4PiecePanel.add(ship4Piece);
        ship4PiecePanel.add( new Spacing(0,5));
        ship4PiecePanel.add(AddButtonShip4Piece);
        AddButtonShip4Piece.setFocusPainted(false);
        AddButtonShip4Piece.addActionListener(this);
        AddButtonShip4Piece.setActionCommand("+ship4Piece");
        AddButtonShip4Piece.setForeground(Color.decode("#e4ede6"));
        ship4PiecePanel.add(SubButtonShip4Piece);
        SubButtonShip4Piece.setFocusPainted(false);
        SubButtonShip4Piece.addActionListener(this);
        SubButtonShip4Piece.setActionCommand("-ship4Piece");
        SubButtonShip4Piece.setForeground(Color.decode("#e4ede6"));
        contentMiddlePanel.add( ship4PiecePanel);


        JPanel ship3PiecePanel = new JPanel();
        ship3PiecePanel.setLayout(new FlowLayout());
        ship3PiecePanel.setBackground(Color.darkGray);
        ship3PiecePanel.add( new Spacing(0,5));
        ship3PiecePanel.add(ship3Piece);
        ship3PiecePanel.add( new Spacing(0,5));
        ship3PiecePanel.add(AddButtonShip3Piece);
        AddButtonShip3Piece.setFocusPainted(false);
        AddButtonShip3Piece.addActionListener(this);
        AddButtonShip3Piece.setActionCommand("+ship3Piece");
        AddButtonShip3Piece.setForeground(Color.decode("#e4ede6"));
        ship3PiecePanel.add(SubButtonShip3Piece);
        SubButtonShip3Piece.setFocusPainted(false);
        SubButtonShip3Piece.addActionListener(this);
        SubButtonShip3Piece.setActionCommand("-ship3Piece");
        SubButtonShip3Piece.setForeground(Color.decode("#e4ede6"));
        contentMiddlePanel.add( ship3PiecePanel);

        JPanel ship2PiecePanel = new JPanel();
        ship2PiecePanel.setLayout(new FlowLayout());
        ship2PiecePanel.setBackground(Color.darkGray);
        ship2PiecePanel.add( new Spacing(0,5));
        ship2PiecePanel.add(ship2Piece);
        ship2PiecePanel.add( new Spacing(0,5));
        ship2PiecePanel.add(AddButtonShip2Piece);
        AddButtonShip2Piece.setFocusPainted(false);
        AddButtonShip2Piece.addActionListener(this);
        AddButtonShip2Piece.setActionCommand("+ship2Piece");
        AddButtonShip2Piece.setForeground(Color.decode("#e4ede6"));
        ship2PiecePanel.add(SubButtonShip2Piece);
        SubButtonShip2Piece.setFocusPainted(false);
        SubButtonShip2Piece.addActionListener(this);
        SubButtonShip2Piece.setActionCommand("-ship2Piece");
        SubButtonShip2Piece.setForeground(Color.decode("#e4ede6"));
        contentMiddlePanel.add(ship2PiecePanel);
    }

    public void refreshValidateButton(){
        if(option.getShipCustomList().isEmpty()){
            validateButton.setBackground(Color.RED);
            validateButton.setText("Need at least 1 ship");
        }
        else{
            validateButton.setBackground(Color.decode("#21bf4c"));
            validateButton.setText("Ok");
        }
    }

    // LISTENER SET
    private Set<OptionListener> optionListener = new HashSet<>();

    public boolean addOptionListener(OptionListener ecouteur) {
        return optionListener.add(ecouteur);
    }

    public void validateButtonPressed() {
        for (OptionListener ecouteurOption : optionListener) {
            ecouteurOption.validateButtonPressed();
        }
    }

    public void repaintOptionPanel(){
        sizeFieldText.setText("Size of the checkboard ="+option.getCheckBoardSize());
        ship2Piece.setText("Submarine = "+option.getNumberOfShipByLenght(2));
        ship3Piece.setText("Navire = "+option.getNumberOfShipByLenght(3));
        ship4Piece.setText("Destroyer = "+option.getNumberOfShipByLenght(4));
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        if(action.equals("Ok")){
            if(option.getShipCustomList().isEmpty()){
                validateButton.setBackground(Color.RED);
                validateButton.setText("Need at least 1 ship");
                return;
            }
            validateButtonPressed();
            return;
        }
        if(action.equals("+")){
            option.setCheckBoardSize(option.getCheckBoardSize()+1);
            sizeFieldText.setText("Size of the checkboard ="+option.getCheckBoardSize());
            refreshValidateButton();
            return;
        }
        if(action.equals("-")){
            option.setCheckBoardSize(option.getCheckBoardSize()-1);
            sizeFieldText.setText("Size of the checkboard ="+option.getCheckBoardSize());
            refreshValidateButton();
            return;
        }
        if(action.equals("+ship3Piece")){
            option.addShipByLenght(3);
            ship3Piece.setText("Navire = "+option.getNumberOfShipByLenght(3));
            refreshValidateButton();
            return;
        }
        if(action.equals("-ship3Piece")){
            option.removeShipByLenght(3);
            ship3Piece.setText("Navire = "+option.getNumberOfShipByLenght(3));
            refreshValidateButton();
            return;
        }
        if(action.equals("+ship2Piece")){
            option.addShipByLenght(2);
            ship2Piece.setText("Submarine = "+option.getNumberOfShipByLenght(2));
            refreshValidateButton();
            return;
        }
        if(action.equals("-ship2Piece")){
            option.removeShipByLenght(2);
            ship2Piece.setText("Submarine = "+option.getNumberOfShipByLenght(2));
            refreshValidateButton();
            return;
        }
        if(action.equals("+ship4Piece")){
            option.addShipByLenght(4);
            ship4Piece.setText("Destroyer = "+option.getNumberOfShipByLenght(4));
            refreshValidateButton();
            return;
        }
        if(action.equals("-ship4Piece")){
            option.removeShipByLenght(4);
            ship4Piece.setText("Destroyer = "+option.getNumberOfShipByLenght(4));
            refreshValidateButton();
            return;
        }


    }
}
