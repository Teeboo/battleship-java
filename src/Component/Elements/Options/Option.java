package Component.Elements.Options;

import Component.Elements.Piece.ShipCustom;

import java.util.LinkedList;

public class Option {

    private int checkBoardSize;
    private String IaDifficulty;
    private LinkedList<ShipCustom> ShipCustomList = new LinkedList<>();
    private int MaxIdenticalShip = 4;
    private int maxCheckerBoardSize = 14;
    private int minCheckerBoardSize = 10;

    // CONSTRUCTOR
    public Option() {
        this.setCheckBoardSize(10);
        this.IaDifficulty = "Normal";
        AddWantedShip();

    }

    public void clearOption(){
        ShipCustomList.clear();
        AddWantedShip();
    }

    public void AddWantedShip(){
        ShipCustomList.add(new ShipCustom(4));
        ShipCustomList.add(new ShipCustom(3));
        ShipCustomList.add(new ShipCustom(2));
    }

    public int getNumberOfShipByLenght(int lengt){
        int result = 0;
        for(int i = 0 ; i <ShipCustomList.size();i++ ){
            ShipCustom tempShip = ShipCustomList.get(i);
            if(tempShip.getLenght() ==lengt){
                result++;
            }
        }
        return result;
    }

    public void addShipByLenght(int lenght){
        if(getNumberOfShipByLenght(lenght)>=MaxIdenticalShip){
            return;
        }
        ShipCustomList.add(new ShipCustom(lenght));
    }
    public void removeShipByLenght(int lenght){

        if(getNumberOfShipByLenght(lenght)<=0){
            return;
        }
        for(int i = 0 ; i <ShipCustomList.size();i++ ){
            if(ShipCustomList.get(i).getLenght() == lenght){
                ShipCustomList.remove(i);
                return;
            }
        }
    }
    // GETTER SETTER
    public int getCheckBoardSize() {
        return checkBoardSize;
    }

    public LinkedList<ShipCustom> getShipCustomList() {
        return ShipCustomList;
    }

    public int getShipCustomListNumber(){
        return ShipCustomList.size();
    }
    public void setCheckBoardSize(int checkBoardSize) {

        if(checkBoardSize >= maxCheckerBoardSize){
            this.checkBoardSize =maxCheckerBoardSize;
            return;
        }
        if(checkBoardSize <= minCheckerBoardSize){
            this.checkBoardSize = minCheckerBoardSize;
            return;
        }

        this.checkBoardSize = checkBoardSize;
    }



    public String getIaDifficulty() {
        return IaDifficulty;
    }

    public void setIaDifficulty(String iaDifficulty) {
        IaDifficulty = iaDifficulty;
    }
}
