package Component.Elements.Piece;

import Component.Elements.Coordinate.Coordinate;

import java.awt.*;

public class Piece {

    private Coordinate coordinate;
    private boolean placed = false;
    private boolean destroyed = false;

    // CONSTRUCTOR

    public Piece(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
    public Piece() {
    }



    // FUNCTION


    // GETTER SETTER

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public boolean isPlaced() {
        return placed;
    }

    public void setPlaced(boolean placed) {
        this.placed = placed;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    @Override
    public String toString() {
        return "Piece "+(char )this.getCoordinate().getX()+","+this.getCoordinate().getY();
    }
}
