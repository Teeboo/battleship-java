package Component.Elements.Piece;

import java.util.LinkedList;

public class Ship3Piece {
    private Piece firstPiece;
    private Piece secondPiece;
    private Piece thirdPiece;
    private LinkedList PieceList = new LinkedList<Piece>();

   // CONSTRUCTOR

    public Ship3Piece(Piece firstPiece, Piece secondPiece,Piece thirdPiece) {
        this.firstPiece = firstPiece;
        this.secondPiece = secondPiece;
        this.thirdPiece = thirdPiece;
        PieceList.add(firstPiece);
        PieceList.add(secondPiece);
        PieceList.add(thirdPiece);
    }

    // FUNCTION


    public void AddPieceToLinkedList(LinkedList pieceList){


    }

    // GETTER SETTER
    public Piece getFirstPiece() {
        return firstPiece;
    }

    public void setFirstPiece(Piece firstPiece) {
        this.firstPiece = firstPiece;
    }

    public Piece getSecondPiece() {
        return secondPiece;
    }

    public void setSecondPiece(Piece secondPiece) {
        this.secondPiece = secondPiece;
    }

    public Piece getThirdPiece() {
        return thirdPiece;
    }

    public void setThirdPiece(Piece thirdPiece) {
        this.thirdPiece = thirdPiece;
    }

    public LinkedList getPieceList() {
        return PieceList;
    }

}

