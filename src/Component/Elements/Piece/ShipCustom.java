package Component.Elements.Piece;

import java.util.LinkedList;

public class ShipCustom {

    private LinkedList PieceList = new LinkedList<Piece>();
    private int Lenght;

    private int MaxLenght = 10;
    private int MinLenght = 2;

    private boolean destroyed = false;

    // CONSTRUCTOR

    public void clearPieceList(){
        PieceList.clear();
        PieceList.removeAll(PieceList);
    }
    public ShipCustom(int lenght) {
        if(lenght>MaxLenght){
            lenght = MaxLenght;
        }
        if(lenght <= MinLenght){
            lenght = MinLenght;
        }
        Lenght = lenght;
    }

    // FUNCTION

    public void AddPieceToShipCustom(LinkedList<Piece> pieceList){

        for(int i = 0 ; i < pieceList.size();i++){
            PieceList.add(pieceList.get(i));
        }
    }


    // GETTER SETTER

    public LinkedList getPieceList() {
        return PieceList;
    }

    public void setPieceList(LinkedList pieceList) {
        PieceList = pieceList;
    }

    public int getLenght() {
        return Lenght;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }
}
