package Component.Elements.Piece;

import Component.Elements.Square.SquareGRectangle;

import java.util.LinkedList;

public class Ship2Piece {

    private Piece firstPiece;
    private Piece secondPiece;
    private LinkedList PieceList = new LinkedList<Piece>();

    public Ship2Piece(Piece firstPiece, Piece secondPiece) {
        this.firstPiece = firstPiece;
        this.secondPiece = secondPiece;
        PieceList.add(firstPiece);
        PieceList.add(secondPiece);

    }

    public Piece getFirstPiece() {
        return firstPiece;
    }

    public void setFirstPiece(Piece firstPiece) {
        this.firstPiece = firstPiece;
    }

    public Piece getSecondPiece() {
        return secondPiece;
    }

    public void setSecondPiece(Piece secondPiece) {
        this.secondPiece = secondPiece;
    }

    public LinkedList getPieceList() {
        return PieceList;
    }

}
