package Component.Elements.Square;

import Component.Elements.Coordinate.Coordinate;
import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import javax.swing.border.Border;


/**
 * This class is for creating visual Square JPanel with multiple function to manipulate.
 * Each Square object created get Coordinate attributed to.
 */
public class SquareGRectangle extends JPanel {

    /**
     * Square Parent class.
     */
    private Square square;
    /**
     * Form of this class.
     */
    private Form form;
    /**
     * Image icon used on this class.
     */
    JLabel pic = new JLabel();
    /**
     * Border used in Graphical Square creation
     */
    Border border = BorderFactory.createLineBorder(Color.black);


    // CONSTRUCTOR

    /**
     * Create a graphical Square with custom sizing
     * @param square The non-graphical square parent class
     */
    public SquareGRectangle(Square square) {
        this.square = square;
        this.form = Form.Square;
        initComponents();

    }

    // GETTER SETTER

     /**
     * @return The Form of the class.
     */

    public Form getForm() {
        return form;
    }

    /**
     * Used to get the square color.
     * @return The color of the parent class Square.
     */
    public Color getSquareColor() {
        return square.getSquareColor();
    }

    /**
     * Used to change Color variable from the parent class Square.
     * @param color The color you want to set the square to.
     */
    public void changeColor(Color color){
        square.setSquareColor(color);
    }

    /**
     * Used to see if the square is usable.
     * @return The status of Usable (Boolean) variable from the parent class Square.
     */
    public boolean isUsable() {
        return square.isUsable();
    }

    /**
     * Used to see if the square is already taken.
     * @return The status of Taken (Boolean) variable from the parent class Square.
     */
    public boolean isTaken() {
        return square.isTaken();
    }

    /**
     * Used to see if the square have the "Proximity" status.
     * @return The status of Proximity (Boolean) variable from the parent class Square.
     */
    public boolean isProximityTaken(){
        return square.isProximity();
    }

    /**
     * Used to make the square on proximity (Boolean) variable "true" from the parent class Square.
     */
    public void setProximityTrue(){
        square.setProximity(true);
    }

    /**
     * Used to make the square on proximity (Boolean) variable "False" from the parent class Square.
     */
    public void setProximityFalse(){
        square.setProximity(true);
    }

    /**
     * Used to get Coordinate variable from this class.
     * @return The Coordinate from this class.
     */
    public Coordinate getCoordinate(){
        return square.getCoordinate();
    }

    /**
     * Used to set Taken boolean variable to "true".
     */
    public void takeSquare(){
        square.setTaken(true);
    }

    /**
     * Used to set Taken boolean variable to "false".
     */
    public void untakeSquare(){
        square.setTaken(false);
    }

    // FUNCTION

    /**
     *  Used to get Coordinate from this class formated in a LinkedList.
     * @return The Linkedlist containing the coordinate from this object.
     */
    public LinkedList GetCoordinateAsLinkedList(){
        return square.GetCoordinateAsLinkedList();
    }


    /**
     * Used to set Usable and Unable variable to false.
     */

    public void disableButton(){
        square.setUsable(false);
        this.setEnabled(false);
    }

    /**
     * Used to apply red cross image icon to this JPanel Square
     * @param lenght The size category of the icon that need to put added.
     */
    public void applyMissImage(String lenght){
        if(lenght=="Normal"){
            ImageIcon missImage = new ImageIcon("src/Component/image/drop.png");
            missImage.getImage().getScaledInstance(this.getWidth(),this.getHeight(),Image.SCALE_SMOOTH);
            pic.setIcon(missImage);
            pic.setPreferredSize(this.getSize());
            this.add(pic);

        } else if(lenght=="Lower"){
            ImageIcon missImage = new ImageIcon("src/Component/image/dropLower.png");
            missImage.getImage().getScaledInstance(this.getWidth(),this.getHeight(),Image.SCALE_SMOOTH);
            pic.setIcon(missImage);
            pic.setPreferredSize(this.getSize());
            this.add(pic);
        }

    }

    /**
     * Used to apply Ship image icon to this JPanel Square
     * @param lenght The size category of the icon that need to put added.
     */

    public void applyShipImage(String lenght){
        if(lenght=="Normal"){

            ImageIcon missImage = new ImageIcon("src/Component/image/ShipIcon.png");
            pic.setIcon(missImage);
            this.add(pic);

        }else if(lenght=="Lower"){

            ImageIcon missImage = new ImageIcon("src/Component/image/ShipLower.png");
            pic.setIcon(missImage);
            this.add(pic);
        }

    }
    /**
     * Used to apply Ship on fire image icon to this JPanel Square
     * @param lenght The size category of the icon that need to put added.
     */
    public void applyShipFireImage(String lenght){

        if(lenght=="Normal"){

            ImageIcon missImage = new ImageIcon("src/Component/image/ShipFire.png");
            missImage.getImage().getScaledInstance(this.getWidth(),this.getHeight(),Image.SCALE_SMOOTH);
            pic.setIcon(missImage);
            pic.setPreferredSize(this.getSize());
            this.add(pic);

        }else if(lenght=="Lower"){

            ImageIcon missImage = new ImageIcon("src/Component/image/ShipFireLower.png");
            missImage.getImage().getScaledInstance(this.getWidth(),this.getHeight(),Image.SCALE_SMOOTH);
            pic.setIcon(missImage);
            pic.setPreferredSize(this.getSize());
            this.add(pic);
        }

    }

    /**
     * Used to initiate graphical construction of this Square Element with custom sizing - Background color - Border.
     */
    public void initComponents(){
        this.setBackground(square.getSquareColor());
        this.setPreferredSize(new Dimension(56, 56));
        this.setMaximumSize(new Dimension(56, 56));
        this.setBorder(border);
    }

    /**
     * Used to resize this graphical square element with custom sizing.
     * @param size the size of the element to want to resize.
     */
    public void resizeSquare(int size){
        this.setPreferredSize(new Dimension(size,size));
        this.setMaximumSize(new Dimension(size,size));
    }

    /**
     * Used to redefine this object ToString function.
     * @return String coordinate of this object.
     */
    @Override
    public String toString() {
        return ""+getCoordinate();
    }
}
