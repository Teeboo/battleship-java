package Component.Elements.Square;

import Component.Elements.Button.RoundButton;
import Component.Elements.Coordinate.Coordinate;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.LinkedList;

public class SquareGCircular extends JPanel {

    private Square square;
    private Form form;


    @Override
    protected void paintComponent(Graphics g) {
        g.drawOval(0, 0, g.getClipBounds().width, g.getClipBounds().height);
        g.setColor(Color.blue);
    }

    // GETTER

    public Form getForm() {
        return form;
    }

    public Color getSquareColor() {
        return square.getSquareColor();
    }

    public boolean isUsable() {
        return square.isUsable();
    }

    public boolean isTaken() {
        return square.isTaken();
    }

    // FUNCTION

    public Coordinate getCoordinate(){
        return square.getCoordinate();
    }
    public LinkedList GetCoordinateAsLinkedList(){
        return square.GetCoordinateAsLinkedList();
    }

    public void changeColor(Color color){
        square.setSquareColor(color);
    }

    public void disableButton(){
        square.setUsable(false);
        this.setEnabled(false);
    }
    public void takeSquare(){
        square.setTaken(true);
    }
    public void initComponents(){

        this.setBackground(square.getSquareColor());
        //this.setBackground(Color.BLUE);
    }

    // CONSTRUCTOR
    public SquareGCircular(Square square) throws HeadlessException {
        this.square = square;
        this.form = Form.Circle;
        initComponents();

        this.setPreferredSize(new Dimension(50, 50));
        this.setMaximumSize(new Dimension(50, 50));



    }



}
