package Component.Elements.Square;

import Component.Elements.Coordinate.Coordinate;

import java.awt.*;
import java.util.LinkedList;

public class Square {

    private Coordinate coordinate;
    private Color SquareColor;
    private boolean usable = true;
    private boolean taken = false;
    private boolean proximity = false;

    // GETTER SETTER

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Color getSquareColor() {
        return SquareColor;
    }

    public void setSquareColor(Color squareColor) {
        SquareColor = squareColor;
    }

    public boolean isUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public boolean isProximity() {
        return proximity;
    }

    public void setProximity(boolean proximity) {
        this.proximity = proximity;
    }

    // CONSTRUCTOR

    public Square(Coordinate coordinate) {
        this.coordinate = coordinate;
        this.SquareColor = Color.white;
    }

    public Square(Coordinate coordinate, Color squareColor) {
        this.coordinate = coordinate;
        SquareColor = squareColor;
    }

    // FUNCTION

    public LinkedList GetCoordinateAsLinkedList(){
        LinkedList List = new LinkedList<>();
        List.add(coordinate.getX());
        List.add(coordinate.getY());
        return List;
    }


}
