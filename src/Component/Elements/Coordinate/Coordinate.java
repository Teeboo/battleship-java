package Component.Elements.Coordinate;


import java.util.Objects;

public class Coordinate {

    public char x;
    public int y;

    // CONSTRUCTOR

    public Coordinate(char x, int y) {
        if(x < 65){
            x = 65;
        }
        this.x = x;
        if(y <= 0){
            y = 1;
        }
        this.y = y;
    }
    public Coordinate() {
    }

    // FUNCTION

    public Coordinate getHorizontalCoord(Coordinate coord,String way){

        char x = (char) coord.getX();
        int y = coord.getY();

        if(way.equals("right")){
            return new Coordinate((char) ((char) x+1), y);
        }
        else{
            return new Coordinate((char) ((char) x-1), y);
        }
    }


    public Coordinate getVerticalCoord(Coordinate coord,String way){

        char x = (char) coord.getX();
        int y = coord.getY();

        if(way.equals("top")){
            return new Coordinate((char) x, y+1);
        }
        else{
            return new Coordinate((char) x, y-1);
        }
    }


    // GETTER SETTER

    public int getX() {
        return x;
    }

    public void setX(char x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
