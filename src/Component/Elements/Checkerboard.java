package Component.Elements;

import Component.Elements.Coordinate.Coordinate;
import Component.Elements.Square.Form;
import Component.Elements.Square.Square;
import Component.Elements.Square.SquareGCircular;
import Component.Elements.Square.SquareGRectangle;
import Tooltip.Alphabet;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class Checkerboard extends JPanel {

    private Form form;
    private final int Xlength;
    private final int Ylength;
    private Color color;
    public LinkedList SquareList = new LinkedList<SquareGRectangle>();

    public Checkerboard(int Xlength, int Ylength,Form form,Color color) {
        this.Xlength = Xlength;
        this.Ylength = Ylength;
        this.form = form;
        this.color = color;

        initComponent();
    }

    public void initComponent(){

        this.setLayout(new GridLayout(Ylength,Xlength));
        char MaxXlengthLetter = Alphabet.GetLetterFromInt(Xlength);

        for(int i = Ylength ; i >= 1 ; i--){
            for(char c = 'A'; c <= MaxXlengthLetter; ++c){

                if(form == Form.Square){
                    SquareGRectangle SquareGRectangle = new SquareGRectangle(new Square(new Coordinate(c,i),color));
                    this.add(SquareGRectangle);
                    SquareList.add(SquareGRectangle);
                }
                else if (form == Form.Circle) {
                    SquareGCircular squareGCircular = new SquareGCircular(new Square(new Coordinate(c,i),color));
                    this.add(squareGCircular);
                    SquareList.add(squareGCircular);
                }
            }
        }
    }

    public void clearCheckerBoard(){
        for(int i = 0 ; i < SquareList.size(); i++){
            SquareGRectangle tempsSquare = (SquareGRectangle) SquareList.get(i);
            tempsSquare.untakeSquare();
            tempsSquare.setProximityFalse();
            tempsSquare.disableButton();
            tempsSquare.setBackground(Color.decode("#2660d4"));
        }
    }
    public boolean CheckIfSquareExist(Coordinate coord1){

        boolean exist = false;

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square =(SquareGRectangle) this.getSquareList().get(i);
            if((square.getCoordinate().equals(coord1) && !square.isTaken() && !square.isProximityTaken())){
                return true;

            }else{
                exist =  false;
            }
        }
        return exist;
    }

    public void paintSquareByCoordonate(Coordinate coord,Color color){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.getCoordinate().equals(coord)){
                square.setBackground(color);
            }
        }
    }
    public void paintImageSquareByCoordonate(Coordinate coord){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.getCoordinate().equals(coord)){
                if(this.Xlength >= 11){
                    square.applyShipImage("Lower");
                }else{
                    square.applyShipImage("Normal");
                }

            }
        }
    }
    public void paintFireImageSquareByCoordonate(Coordinate coord){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.getCoordinate().equals(coord)){
                if (this.Xlength >= 11) {
                    square.applyShipFireImage("Lower");

                }else {
                    square.applyShipFireImage("Normal");
                }

            }
        }
    }

    public void SetProximitySquareByCoordinate(Coordinate coord){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.getCoordinate().equals(coord)){
                square.setProximityTrue();
            }
        }

    }
    public void takeSquareByCoordonate(Coordinate coord){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.getCoordinate().equals(coord)){
                square.takeSquare();
                Coordinate actualCoord = square.getCoordinate();

                this.SetProximitySquareByCoordinate(actualCoord.getHorizontalCoord(actualCoord,"right"));
                this.SetProximitySquareByCoordinate(actualCoord.getHorizontalCoord(actualCoord,"left"));
                this.SetProximitySquareByCoordinate(actualCoord.getVerticalCoord(actualCoord,"top"));
                this.SetProximitySquareByCoordinate(actualCoord.getVerticalCoord(actualCoord,"down"));

            }
        }
    }
    public void takeSquareByCoordonateList(LinkedList<Coordinate> CoordList){

        for(int i = 0 ; i < CoordList.size();i++){
            Coordinate tempCoord = CoordList.get(i);
            takeSquareByCoordonate(tempCoord);
        }

    }
    public void paintAllSquareIfNotTaken(Color color){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(!square.isTaken()){
                square.setBackground(color);
            }
        }
    }
    public void paintAllTakenSquare(Color color){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.isTaken()){
                square.setBackground(color);
            }
        }
    }
    // TODO IMPORT OPTION
    public void paintAllTakenSquareWithImage(){

        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.isTaken()){
                //square.applyShipImage();
            }
        }
    }
    public void resizeCheckboard(int size){
        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            square.resizeSquare(size);
        }
    }
    public LinkedList getSquareList() {
        return SquareList;
    }

    public int GetNumberOftakenSquare(){
        int result = 0;
        for(int i = 0 ; i < this.getSquareList().size(); i++){
            SquareGRectangle square = (SquareGRectangle) this.getSquareList().get(i);
            if(square.isTaken()){
                result++;
            }
        }
        return result;
    }
}
