package Component.Listener;

public interface OptionListener {
    void validateButtonPressed();
}
