package Component.Listener;

public interface MenuListener {

        void PlayButtonPressed();
        void OptionButtonPressed();
        void QuitButtonPressed();


}