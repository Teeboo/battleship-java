package Component.Listener;

public interface GameplayListener {
    void ReturnButtonPressedFromGameplay();
    void GameFinished(String winnerName);
}
