package Component.Listener;

public interface PiecePlacementListener {
    void ReturnButtonPressedFromPiecePlacement();
    void ValidateButtonPressedFromPiecePlacement();
}
