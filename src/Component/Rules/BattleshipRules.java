package Component.Rules;

import Component.Elements.Deck.ShipDeck;
import Component.Elements.Piece.Piece;
import Component.Elements.Piece.Ship2Piece;
import Component.Elements.Piece.Ship3Piece;

import java.util.LinkedList;

public class BattleshipRules {



    public boolean CheckVictory(LinkedList<Piece> pieceList){

        boolean victory = true;

        for(int i = 0 ; i < pieceList.size(); i++){

            Piece tmpPiece = pieceList.get(i);
            if(!tmpPiece.isDestroyed()){
                victory = false;
            }
        }
        return victory;
    }
}
